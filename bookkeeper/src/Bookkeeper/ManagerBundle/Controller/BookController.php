<?php

namespace Bookkeeper\ManagerBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Bookkeeper\ManagerBundle\Entity\Book;
use Bookkeeper\ManagerBundle\Form\BookType;
use Symfony\Component\HttpFoundation\Response;

class BookController extends Controller
{
    /**
     * @Route("/", name="book")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $books = $em->getRepository('BookkeeperManagerBundle:Book')->findAll();
        return $this->render('BookkeeperManagerBundle:Book:index.html.twig', array(
            'books'=>$books
        ));
    }

    /**
     * @Route("/show/{id}", name="book_show")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $book =$em->getRepository('BookkeeperManagerBundle:Book')->find($id);

        $delete_form = $this->createFormDelete($id);

        return $this->render('BookkeeperManagerBundle:Book:show.html.twig', array(
            'book' => $book,
            'delete_form' => $delete_form->createView()
        ));
    }

    /**
     * @Route("/new", name="book_new")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function newAction()
    {
        $book = new Book();
        $form = $this->createFormFor($book, 'create', 'POST');
        return $this->render('BookkeeperManagerBundle:Book:new.html.twig', array(
            'form'=>$form->createView()
        ));
    }


    /**
     * Helper method that creates a form
     * @param $book
     * @param $for
     * @param $method
     * @return Form
     */
    public function createFormFor($book, $for, $method){

        $form = $this->createForm(new BookType(), $book, array(
            'action'=>$this->generateUrl('book_'.$for, array('id'=>$book->getId())),
            'method'=>$method
        ));
        $form->add('submit', 'submit', array('label'=>ucfirst($for.' Book')));
        return $form;
    }

    /**
     * @Route("/create", name="book_create")
     * @Method("POST")
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        $book = new Book();
        $form = $form = $this->createFormFor($book, 'create', 'POST');

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $em->persist($book);
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg', 'Your book has been created!');

            return $this->redirect($this->generateUrl('book_show', array('id'=>$book->getId())));
        }

        $this->get('session')->getFlashBag()->add('msg', 'Something went wrong!');

        return $this->render('BookkeeperManagerBundle:Book:new.html.twig', array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route("/edit/{id}", name="book_edit")
     * @param $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookkeeperManagerBundle:Book')->find($id);
        $form = $this->createFormFor($book,'update', 'PUT');

        return $this->render('BookkeeperManagerBundle:Book:edit.html.twig', array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route("/update/{id}", name="book_update")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function updateAction(Request $request,$id)
    {
        $em = $this->getDoctrine()->getManager();
        $book = $em->getRepository('BookkeeperManagerBundle:Book')->find($id);

        $form = $this->createFormFor($book,'update', 'PUT');

        $form->handleRequest($request);

        if($form->isValid()){
            $em->flush();

            $this->get('session')->getFlashBag()->add('msg', 'Your book has been updated!');

            return $this->redirect($this->generateUrl('book_show', array('id'=>$book->getId())));
        }

        $this->get('session')->getFlashBag()->add('msg', 'Something went wrong!');
        return $this->render('BookkeeperManagerBundle:Book:edit.html.twig', array(
            'form'=>$form->createView()
        ));
    }

    /**
     * @Route("/delete/{id}", name="book_delete")
     * @param Request $request
     * @param $id
     * @return Response
     */
    public function deleteAction(Request $request,$id)
    {
        $delete_form = $this->createFormDelete($id);

        $delete_form->handleRequest($request);

        if($delete_form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $book = $em->getRepository('BookkeeperManagerBundle:Book')->find($id);
            $em->remove($book);
            $em->flush();
        }

        $this->get('session')->getFlashBag()->add('msg', 'Your book has been deleted!');

        return $this->redirect($this->generateUrl('book'));
    }

    /**
     * Helper function that creates a delete form
     * @param $id
     * @return Form
     */
    public function createFormDelete($id)
    {
        $delete_form = $this->createFormBuilder()
            ->setAction($this->generateUrl('book_delete', array('id'=>$id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label'=>'Delete Book'))
            ->getForm();
        return $delete_form;
    }
}
