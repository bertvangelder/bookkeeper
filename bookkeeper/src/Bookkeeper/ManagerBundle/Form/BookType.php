<?php
/**
 * Created by PhpStorm.
 * User: Bert
 * Date: 27/11/2016
 * Time: 16:28
 */

namespace Bookkeeper\ManagerBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BookType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('title')->add('description')->add('pages');
    }

    public function setDefaults(OptionsResolver $resolver)
    {
       $resolver->setDefaults(array("data_class"=>'Bookkeeper\ManagerBundle\BookkeeperManagerBundle\Entity\Book'));
    }

    public function getName()
    {
        return 'bookkeeper_managerbundle_book';
    }
}